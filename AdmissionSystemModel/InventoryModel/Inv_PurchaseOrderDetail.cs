//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventorySystemModel.InventoryModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Inv_PurchaseOrderDetail
    {
        public int DetailID { get; set; }
        public int Master_ID { get; set; }
        public int Product_ID { get; set; }
        public Nullable<decimal> Qty { get; set; }
        public int ProductType_ID { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string DetailDescription { get; set; }
    
        public virtual Inv_PurchaseOrderInfo Inv_PurchaseOrderInfo { get; set; }
    }
}
