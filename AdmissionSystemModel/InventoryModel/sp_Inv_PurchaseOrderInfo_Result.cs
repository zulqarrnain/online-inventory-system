//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventorySystemModel.InventoryModel
{
    using System;
    
    public partial class sp_Inv_PurchaseOrderInfo_Result
    {
        public int PurchaseOrderID { get; set; }
        public Nullable<System.DateTime> EntryDate { get; set; }
        public Nullable<int> OrderNr { get; set; }
        public int Gen_Party_ID { get; set; }
        public int Gen_Broker_ID { get; set; }
        public string PaymentTerms { get; set; }
        public int Employee_ID { get; set; }
        public int Session_ID { get; set; }
        public string OrderStatus { get; set; }
        public Nullable<bool> InActive { get; set; }
    }
}
