//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventorySystemModel.InventoryModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class User_LoginInfo
    {
        public int LoginID { get; set; }
        public int LoginCode { get; set; }
        public string LoginTitle { get; set; }
        public string Password { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }
        public Nullable<bool> InActive { get; set; }
        public string Description { get; set; }
        public int GroupRights_ID { get; set; }
        public int Employee_ID { get; set; }
    }
}
