//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventorySystemModel.InventoryModel
{
    using System;
    
    public partial class sp_Inv_ProductCategoryInfo_Result
    {
        public int ProductCategoryCode { get; set; }
        public string ProductCategoryTitle { get; set; }
        public string ProductCategoryTitleInUrdu { get; set; }
        public bool InActive { get; set; }
        public string Description { get; set; }
    }
}
