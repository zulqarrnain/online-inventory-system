//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventorySystemModel.InventoryModel
{
    using System;
    
    public partial class sp_Acc_Vouchers_Result
    {
        public int VoucherID { get; set; }
        public int Code { get; set; }
        public string VType { get; set; }
        public System.DateTime VDate { get; set; }
        public string ManualNr { get; set; }
        public Nullable<bool> IsReadOnly { get; set; }
        public Nullable<bool> IsPosted { get; set; }
        public Nullable<bool> Validate { get; set; }
        public string Description { get; set; }
        public int Branch_ID { get; set; }
        public int Session_ID { get; set; }
    }
}
