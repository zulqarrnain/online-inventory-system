//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventorySystemModel.InventoryModel
{
    using System;
    
    public partial class sp_Gen_VehicleTypeInfo_Result
    {
        public int VehicleID { get; set; }
        public int VehicleCode { get; set; }
        public string VehicleTypeTitle { get; set; }
        public string VehicleTypeTitleInUrdu { get; set; }
        public bool InActive { get; set; }
        public string Description { get; set; }
    }
}
