﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdmissionSystemModel.InventoryModel
{
 public partial class StudentSession
    {

        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }

        public int addata(StudentSession obj)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    context.StudentSessions.Add(obj);
                    context.SaveChanges();
                    return obj.StudentSessionID;
                }
            }
            catch (Exception ex)
            {
             
                return 0;
            }
        }



        public int UpdateData(StudentSession obj)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var result =context.StudentSessions.SingleOrDefault(x=>x.StudentSessionID==obj.StudentSessionID);
                    if (result!=null)
                    {
                        result.title = obj.title;
                        result.Description = obj.Description;
                        context.SaveChanges();
                        return result.StudentSessionID;
                    }
                    return result.StudentSessionID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }


        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var result = context.StudentSessions.SingleOrDefault(x => x.StudentSessionID == id);
                    if (result != null)
                    {
                        context.StudentSessions.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public StudentSession getAllSession(int id)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    return context.StudentSessions.SingleOrDefault(x=>x.StudentSessionID==id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<StudentSession> getAllSession()
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                  return  context.StudentSessions.ToList();
                 
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public List< StudentSession> checkDuplicate(int id ,string title)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    if (id>0)
                    {
                        return context.StudentSessions.Where(x => x.title == title && x.StudentSessionID != id).ToList();

                    }
                    else
                    {
                        return context.StudentSessions.Where(x => x.title == title ).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
    }
}
