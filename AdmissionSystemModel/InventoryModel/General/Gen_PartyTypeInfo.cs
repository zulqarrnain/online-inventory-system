﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventorySystemModel.InventoryModel
{
  public partial  class Gen_PartyTypeInfo
    {


        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }
        public string system { get; set; }
        public string IP { get; set; }
 

        public int addata(Gen_PartyTypeInfo obj)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    //  obj.PartyTypeID = new Login().GetUser().PartyTypeID;
                    context.Gen_PartyTypeInfo.Add(obj);
                    context.SaveChanges();
                    return obj.PartyTypeID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }



        public int UpdateData(Gen_PartyTypeInfo obj)
        {
            try
            {

                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_PartyTypeInfo.SingleOrDefault(x => x.PartyTypeID == obj.PartyTypeID);
                    if (result != null)
                    {
                        result.PartyTypeTitle = obj.PartyTypeTitle;
                        result.PartyTypeTitleInUrdu = obj.PartyTypeTitleInUrdu;
                        result.Code = obj.Code;
                        result.Initial = obj.Initial;
                        result.Description = obj.Description;
                        result.InActive = obj.InActive;

                        context.SaveChanges();
                        return result.PartyTypeID;
                    }
                    return result.PartyTypeID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }



        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_PartyTypeInfo.SingleOrDefault(x => x.PartyTypeID == id);
                    if (result != null)
                    {
                        context.Gen_PartyTypeInfo.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public Gen_PartyTypeInfo getAlldataByID(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_PartyTypeInfo.SingleOrDefault(x => x.PartyTypeID == id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<Gen_PartyTypeInfo> getAllPartytypedata()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_PartyTypeInfo.ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }




        public List<Gen_PartyTypeInfo> checkDuplicate(int id, string title)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    if (id > 0)
                    {
                        return context.Gen_PartyTypeInfo.Where(x => x.PartyTypeTitle == title && x.PartyTypeID != id).ToList();

                    }
                    else
                    {
                        return context.Gen_PartyTypeInfo.Where(x => x.PartyTypeTitle == title).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
    }
}
