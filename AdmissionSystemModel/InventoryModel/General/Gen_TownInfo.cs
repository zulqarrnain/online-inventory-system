﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace InventorySystemModel.InventoryModel
{
     public partial class Gen_TownInfo
    {
        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }
        public string system { get; set; }
        public string IP { get; set; }

        public int addata(Gen_TownInfo obj)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    context.Gen_TownInfo.Add(obj);
                    context.SaveChanges();
                    return obj.TownID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        public int UpdateData(Gen_TownInfo obj)
        {
            try
            {

                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_TownInfo.SingleOrDefault(x => x.TownID == obj.TownID);
                    if (result != null)
                    {
                        result.TownTitle = obj.TownTitle;
                        result.Code = obj.Code;
                        result.Description = obj.Description;
                        result.Territory_ID = obj.Territory_ID;
                        result.TownTitleInUrdu = obj.TownTitleInUrdu;
                        result.Description = obj.Description;


                        context.SaveChanges();
                        return result.TownID;
                    }
                    return result.TownID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_TownInfo.SingleOrDefault(x => x.TownID == id);
                    if (result != null)
                    {
                        context.Gen_TownInfo.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public Gen_TownInfo getAlldataByID(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_TownInfo.SingleOrDefault(x => x.TownID == id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public List<Gen_TownInfo> GetAllTownInfo()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_TownInfo.ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public List<Gen_TownInfo> checkDuplicate(int id, string title)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    if (id > 0)
                    {
                        return context.Gen_TownInfo.Where(x => x.TownTitle == title && x.TownID != id).ToList();

                    }
                    else
                    {
                        return context.Gen_TownInfo.Where(x => x.TownTitle == title).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public IEnumerable<SelectListItem> GetAllTerritory()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var list = context.Gen_TerritoryInfo.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Part--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.TerritoryTitle, Value = item.TerritoryID.ToString() });
                    }


                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
    }
}
