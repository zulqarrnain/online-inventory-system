﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace InventorySystemModel.InventoryModel
{
 public  partial class Gen_PartyInfo
    {

        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }
        public string system { get; set; }
        public string IP { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }


        public int addata(Gen_PartyInfo obj)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    //  obj.PartyID = new Login().GetUser().PartyID;
                    context.Gen_PartyInfo.Add(obj);
                    context.SaveChanges();
                    return obj.PartyID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }


        public IEnumerable<SelectListItem> GetRegion()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var list = context.Gen_RegoinInfo.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Region--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.RegionTitle, Value = item.RegionID.ToString() });
                    }
                    
                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public List<Gen_TerritoryInfo> GetTerritory(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                return context.Gen_TerritoryInfo.Where(x=>x.Region_ID==id).ToList(); 

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public List<Gen_TownInfo> GetTown(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_TownInfo.Where(x=>x.Territory_ID==id).ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }



        public List<Gen_SectorInfo> GetSector( int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_SectorInfo.Where(x=>x.Town_ID==id).ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public int UpdateData(Gen_PartyInfo obj)
        {
            try
            {

                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_PartyInfo.SingleOrDefault(x => x.PartyID == obj.PartyID);
                    if (result != null)
                    {
                        result.PartyCode = obj.PartyCode ;
                        result.PartyTitle = obj.PartyTitle;
                        result.PartyTitleInUrdu = obj.PartyTitleInUrdu;
                        result.PartyAddress = obj.PartyAddress;
                        result.PartyAddressInUrdu = obj.PartyAddressInUrdu;
                        result.GSTNr = obj.GSTNr;
                        result.NTNNr = obj.NTNNr;
                        result.Email = obj.Email;
                        result.Description = obj.Description;
                        result.PhoneNr = obj.PhoneNr;
                        result.MobileNr = obj.MobileNr;
                        result.Designation = obj.Designation;
                        result.WebAddress = obj.WebAddress;
                        result.ContacePerson = obj.ContacePerson;
                        result.Country = obj.Country;
                        result.Sector_ID = obj.Sector_ID;
                        result.Town_ID = obj.Town_ID;
                        result.Territory_ID = obj.Territory_ID;
                        result.Region_ID = obj.Region_ID;
                        result.Account_ID = obj.Account_ID;
                        result.IsSupplier = obj.IsSupplier;
                        result.CreditLimit = obj.CreditLimit;
                        result.WebAddress = obj.WebAddress;
                        result.LogoPath = obj.LogoPath;

                        context.SaveChanges();
                        return result.PartyID;
                    }
                    return result.PartyID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_PartyInfo.SingleOrDefault(x => x.PartyID == id);
                    if (result != null)
                    {
                        context.Gen_PartyInfo.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public Gen_PartyInfo getAlldataByID(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_PartyInfo.SingleOrDefault(x => x.PartyID == id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<Gen_PartyInfo> getAllCompanydata()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_PartyInfo.Include(x => x.Gen_PartyTypeInfo).ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<Gen_PartyInfo> checkDuplicate(int id, string title)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    if (id > 0)
                    {
                        return context.Gen_PartyInfo.Where(x => x.PartyTitle == title && x.PartyID != id).ToList();

                    }
                    else
                    {
                        return context.Gen_PartyInfo.Where(x => x.PartyTitle == title).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public IEnumerable<SelectListItem> Get_AllPartytype()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var list = context.Gen_PartyTypeInfo.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Part--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.PartyTypeTitle, Value = item.PartyTypeID.ToString() });
                    }


                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


    }
}
