﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;

namespace InventorySystemModel.InventoryModel
{
  public partial  class Gen_BranchInfo
    {

        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }
        public string system { get; set; }
        public string IP { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }


        public int addata(Gen_BranchInfo obj)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    //  obj.BranchID = new Login().GetUser().BranchID;
                    context.Gen_BranchInfo.Add(obj);
                    context.SaveChanges();
                    return obj.BranchID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }



        public int UpdateData(Gen_BranchInfo obj)
        {
            try
            {

                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_BranchInfo.SingleOrDefault(x => x.BranchID == obj.BranchID);
                    if (result != null)
                    {
                        result.BranchTitle = obj.BranchTitle;
                        result.Code = obj.Code;
                        result.Description = obj.Description;
                        result.PhoneNr = obj.PhoneNr;
                        result.MobileNr = obj.MobileNr;
                        result.AddressInUrdu = obj.AddressInUrdu;
                        result.Address = obj.Address;
                        result.BranchTitleInUrdu = obj.BranchTitleInUrdu;
                        result.Company_ID = obj.Company_ID;
                        result.InActive = obj.InActive;
                        result.LogoPath = obj.LogoPath;
                        context.SaveChanges();
                        return result.BranchID;
                    }
                    return result.BranchID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }


        public IEnumerable<SelectListItem> loadCompany()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var list = context.Gen_CompanyInfo.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Company--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.CompanyTitle, Value = item.CompID.ToString() });
                    }


                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_BranchInfo.SingleOrDefault(x => x.BranchID == id);
                    if (result != null)
                    {
                        context.Gen_BranchInfo.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public Gen_BranchInfo getAlldataByID(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_BranchInfo.SingleOrDefault(x => x.BranchID == id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<Gen_BranchInfo> getAllCompanydata()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_BranchInfo.Include(x=>x.Gen_CompanyInfo).ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }




        public List<Gen_BranchInfo> checkDuplicate(int id, string title)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    if (id > 0)
                    {
                        return context.Gen_BranchInfo.Where(x => x.BranchTitle == title && x.BranchID != id).ToList();

                    }
                    else
                    {
                        return context.Gen_BranchInfo.Where(x => x.BranchTitle == title).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

    }
}
