﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace InventorySystemModel.InventoryModel
{
    public partial class Gen_TerritoryInfo
    {
        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }
        public string system { get; set; }
        public string IP { get; set; }

        public int addata(Gen_TerritoryInfo obj)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    context.Gen_TerritoryInfo.Add(obj);
                    context.SaveChanges();
                    return obj.TerritoryID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        public int UpdateData(Gen_TerritoryInfo obj)
        {
            try
            {

                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_TerritoryInfo.SingleOrDefault(x => x.TerritoryID == obj.TerritoryID);
                    if (result != null)
                    {
                        result.TerritoryTitle = obj.TerritoryTitle;
                        result.Code = obj.Code;
                        result.Description = obj.Description;
                        result.Region_ID = obj.Region_ID;
                        result.TerritoryTitleInUrdu = obj.TerritoryTitleInUrdu;
                        result.Description = obj.Description;


                        context.SaveChanges();
                        return result.TerritoryID;
                    }
                    return result.TerritoryID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_TerritoryInfo.SingleOrDefault(x => x.TerritoryID == id);
                    if (result != null)
                    {
                        context.Gen_TerritoryInfo.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public Gen_TerritoryInfo getAlldataByID(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_TerritoryInfo.SingleOrDefault(x => x.TerritoryID == id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public List<Gen_TerritoryInfo> GetAllTerritoryInfo()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_TerritoryInfo.ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public List<Gen_TerritoryInfo> checkDuplicate(int id, string title)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    if (id > 0)
                    {
                        return context.Gen_TerritoryInfo.Where(x => x.TerritoryTitle == title && x.TerritoryID != id).ToList();

                    }
                    else
                    {
                        return context.Gen_TerritoryInfo.Where(x => x.TerritoryTitle == title).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public IEnumerable<SelectListItem> GetAllRegion()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var list = context.Gen_TerritoryInfo.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Part--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.TerritoryTitle, Value = item.Region_ID.ToString() });
                    }


                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


    }
}
