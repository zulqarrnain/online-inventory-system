﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;


namespace InventorySystemModel.InventoryModel
{
 public partial class GLUser
    {
        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }
        public string system { get; set; }
        public string IP { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }


        public GLUser UserLogin(string Username ,string password)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.GLUsers.SingleOrDefault(u=>u.UserName== Username && u.UserPassword== password);
                }
            }
            catch (Exception ex)
            {
             
                return null;
            }
        }



        public int addata(GLUser obj)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                  //  obj.Userid = new Login().GetUser().Userid;
                    context.GLUsers.Add(obj);
                    context.SaveChanges();
                    return obj.Userid;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }



        public int UpdateData(GLUser obj)
        {
            try
            {
            
                using (var context = new TradeProEntities())
                {
                    var result = context.GLUsers.SingleOrDefault(x => x.Userid == obj.Userid);
                    if (result != null)
                    {
                        result.UserName = obj.UserName;
                        result.UserPassword = obj.UserPassword;
                        result.GroupID = obj.GroupID;
                        result.Type = obj.Type;
                        result.Userid = new GLUser().GetUser().Userid;
                        result.Entryby = new GLUser().GetUser().Userid.ToString();
                        result.TimeStamp = DateTime.Now;
                        result.Active = obj.Active;
                        result.PhotoPath = obj.PhotoPath;
                        context.SaveChanges();
                        return result.Userid;
                    }
                    return result.Userid;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        public GLUser GetUser()
        {
            GLUser user = null;
            HttpCookie authCookie = HttpContext.Current.Request.Cookies["UserCookies"];

            if (authCookie != null)
            {
                user = new GLUser();
                user.Userid = Convert.ToInt32((authCookie.Values["Userid"]));
                user.UserName = Convert.ToString(authCookie.Values["UserName"]);
                user.UserPassword = Convert.ToString(authCookie.Values["UserPassword"]);
                user.Type = Convert.ToBoolean(authCookie.Values["Type"]);
                user.system = Convert.ToString(authCookie.Values["system"]);



            }
            return user;
        }
        public IEnumerable<SelectListItem> loadUsergroup()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var list = context.GLUserGroups.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Part--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.GroupTitle, Value = item.GroupID.ToString() });
                    }


                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var result = context.GLUsers.SingleOrDefault(x => x.Userid == id);
                    if (result != null)
                    {
                        context.GLUsers.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public GLUser getAlldataByID(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.GLUsers.SingleOrDefault(x => x.Userid == id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<sp_getAllUsersData_Result> getAllUserdata()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.sp_getAllUsersData().ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }




        public List<GLUser> checkDuplicate(int id, string title)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    if (id > 0)
                    {
                        return context.GLUsers.Where(x => x.UserName == title && x.Userid != id).ToList();

                    }
                    else
                    {
                        return context.GLUsers.Where(x => x.UserName == title).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public string[] userinfofromCookie()
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

            string cookiePath = ticket.CookiePath;
            DateTime expiration = ticket.Expiration;
            bool expired = ticket.Expired;
            bool isPersistent = ticket.IsPersistent;
            DateTime issueDate = ticket.IssueDate;
            string name = ticket.Name;
            string userData = ticket.UserData;
            int version = ticket.Version;
            string[] a = name.Split('|');
            return a;
        }

        public DataTable checkRightUser(string where = "")
        {
            using (var context = new TradeProEntities())
            {
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(@"SELECT     GLUser.type,  GLUser.Userid, GLUser.UserName, GLUser.UserPassword, GLUser.GroupID, GLUser.Active, GLUserGroup.GroupTitle, GLUserGroup.Description, GLUserGroup.Inactive, GLUserGroupDetail.Assign, GLUserGroupDetail.IsEdit, 
                         GLUserGroupDetail.IsDelete, GLUserGroupDetail.IsPrint, GLUserGroupDetail.Isnew, UserForms.FormTitle, UserForms.Formid
                            FROM            GLUser INNER JOIN
                         GLUserGroup ON GLUserGroup.GroupID = GLUser.GroupID INNER JOIN
                         GLUserGroupDetail ON GLUserGroupDetail.UserGroupID = GLUserGroup.GroupID INNER JOIN
                         UserForms ON UserForms.Formid = GLUserGroupDetail.FormsID " + where + "", context.Database.Connection.ConnectionString.ToString()
                             );
                da.Fill(dt);
                return dt;
            }
        }


    }
}
