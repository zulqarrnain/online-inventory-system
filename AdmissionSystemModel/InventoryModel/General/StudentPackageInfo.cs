﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AdmissionSystemModel.InventoryModel
{
 public partial class StudentPackageInfo
    {

        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }

        public int addata(StudentPackageInfo obj)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    context.StudentPackageInfoes.Add(obj);
                    context.SaveChanges();
                    return obj.PackageID;
                }
            }
            catch (Exception ex)
            {
             
                return 0;
            }
        }



        public int UpdateData(StudentPackageInfo obj)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var result =context.StudentPackageInfoes.SingleOrDefault(x=>x.PackageID==obj.PackageID);
                    if (result!=null)
                    {
                        result.PackageTitle = obj.PackageTitle;
                        result.description = obj.description;
                        result.AdmissionFee = obj.AdmissionFee;
                        result.OtherFee = obj.OtherFee;
                        result.TiutionFee = obj.TiutionFee;
                        result.PartID = obj.PartID;
                        context.SaveChanges();
                        return result.PackageID;
                    }
                    return result.PackageID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }

        public IEnumerable<SelectListItem> getPartValue()
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var list = context.StudentPrograms.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Part--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.title, Value = item.ProgramID.ToString() });
                    }


                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var result = context.StudentPackageInfoes.SingleOrDefault(x => x.PackageID == id);
                    if (result != null)
                    {
                        context.StudentPackageInfoes.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public StudentPackageInfo getAlldataByID(int id)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    return context.StudentPackageInfoes.SingleOrDefault(x=>x.PackageID==id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<sp_GetAllPackages_Result> getAllPackages()
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                  return  context.sp_GetAllPackages().ToList();
                 
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public List< StudentPackageInfo> checkDuplicate(int id ,string title)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    if (id>0)
                    {
                        return context.StudentPackageInfoes.Where(x => x.PackageTitle == title && x.PackageID != id).ToList();

                    }
                    else
                    {
                        return context.StudentPackageInfoes.Where(x => x.PackageTitle == title ).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
    }
}
