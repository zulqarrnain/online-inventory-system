﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace InventorySystemModel.InventoryModel
{
    public partial class Gen_SectorInfo
    {
        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }
        public string system { get; set; }
        public string IP { get; set; }

        public int addata(Gen_SectorInfo obj)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    context.Gen_SectorInfo.Add(obj);
                    context.SaveChanges();
                    return obj.SectorID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        public int UpdateData(Gen_SectorInfo obj)
        {
            try
            {

                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_SectorInfo.SingleOrDefault(x => x.SectorID == obj.SectorID);
                    if (result != null)
                    {
                        result.SectorTitle = obj.SectorTitle;
                        result.Code = obj.Code;
                        result.Description = obj.Description;
                        result.Town_ID = obj.Town_ID;
                        result.SectorTitleInUrdu = obj.SectorTitleInUrdu;
                        result.Description = obj.Description;


                        context.SaveChanges();
                        return result.SectorID;
                    }
                    return result.SectorID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_SectorInfo.SingleOrDefault(x => x.SectorID == id);
                    if (result != null)
                    {
                        context.Gen_SectorInfo.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public Gen_SectorInfo getAlldataByID(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_SectorInfo.SingleOrDefault(x => x.SectorID == id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public List<Gen_SectorInfo> GetAllSectorInfo()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_SectorInfo.ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public List<Gen_SectorInfo> checkDuplicate(int id, string title)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    if (id > 0)
                    {
                        return context.Gen_SectorInfo.Where(x => x.SectorTitle == title && x.SectorID != id).ToList();

                    }
                    else
                    {
                        return context.Gen_SectorInfo.Where(x => x.SectorTitle == title).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public IEnumerable<SelectListItem> GetAllTown()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var list = context.Gen_TownInfo.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Part--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.TownTitle, Value = item.TownID.ToString() });
                    }


                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


    }

}
