﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace InventorySystemModel.InventoryModel
{
   public partial class Gen_CompanyInfo
    {


        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }
        public string system { get; set; }
        public string IP { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }


        public int addata(Gen_CompanyInfo obj)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    //  obj.CompID = new Login().GetUser().CompID;
                    context.Gen_CompanyInfo.Add(obj);
                    context.SaveChanges();
                    return obj.CompID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }



        public int UpdateData(Gen_CompanyInfo obj)
        {
            try
            {

                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_CompanyInfo.SingleOrDefault(x => x.CompID == obj.CompID);
                    if (result != null)
                    {
                        result.CompanyTitle = obj.CompanyTitle;
                        result.Email = obj.Email;
                        result.Description = obj.Description;
                        result.PhoneNr = obj.PhoneNr;
                        result.MobileNr = obj.MobileNr;
                        result.GSTNr = obj.GSTNr;
                        result.NTNNr = obj.NTNNr;
                        result.WebAddress = obj.WebAddress;
                        result.LogoPath = obj.LogoPath;

                        context.SaveChanges();
                        return result.CompID;
                    }
                    return result.CompID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
     


        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_CompanyInfo.SingleOrDefault(x => x.CompID == id);
                    if (result != null)
                    {
                        context.Gen_CompanyInfo.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public Gen_CompanyInfo getAlldataByID(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_CompanyInfo.SingleOrDefault(x => x.CompID == id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<Gen_CompanyInfo> getAllCompanydata()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_CompanyInfo.ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }




        public List<Gen_CompanyInfo> checkDuplicate(int id, string title)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    if (id > 0)
                    {
                        return context.Gen_CompanyInfo.Where(x => x.CompanyTitle == title && x.CompID != id).ToList();

                    }
                    else
                    {
                        return context.Gen_CompanyInfo.Where(x => x.CompanyTitle == title).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

     

    }
}
