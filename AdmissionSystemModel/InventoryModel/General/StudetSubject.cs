﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdmissionSystemModel.InventoryModel
{
 public partial class StudetSubject
    {

        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }

        public int addata(StudetSubject obj)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    context.StudetSubjects.Add(obj);
                    context.SaveChanges();
                    return obj.StudetSubjectID;
                }
            }
            catch (Exception ex)
            {
             
                return 0;
            }
        }



        public int UpdateData(StudetSubject obj)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var result =context.StudetSubjects.SingleOrDefault(x=>x.StudetSubjectID==obj.StudetSubjectID);
                    if (result!=null)
                    {
                        result.title = obj.title;
                        result.Description = obj.Description;
                        context.SaveChanges();
                        return result.StudetSubjectID;
                    }
                    return result.StudetSubjectID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }


        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var result = context.StudetSubjects.SingleOrDefault(x => x.StudetSubjectID == id);
                    if (result != null)
                    {
                        context.StudetSubjects.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public StudetSubject getAllSession(int id)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    return context.StudetSubjects.SingleOrDefault(x=>x.StudetSubjectID==id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<StudetSubject> getAllSession()
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                  return  context.StudetSubjects.ToList();
                 
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public List< StudetSubject> checkDuplicate(int id ,string title)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    if (id>0)
                    {
                        return context.StudetSubjects.Where(x => x.title == title && x.StudetSubjectID != id).ToList();

                    }
                    else
                    {
                        return context.StudetSubjects.Where(x => x.title == title ).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
    }
}
