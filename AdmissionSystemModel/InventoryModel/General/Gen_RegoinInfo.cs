﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventorySystemModel.InventoryModel
{
    public partial class Gen_RegoinInfo
    {

        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }
        public string system { get; set; }
        public string IP { get; set; }

        public int addata(Gen_RegoinInfo obj)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    context.Gen_RegoinInfo.Add(obj);
                    context.SaveChanges();
                    return obj.RegionID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        public int UpdateData(Gen_RegoinInfo obj)
        {
            try
            {

                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_RegoinInfo.SingleOrDefault(x => x.RegionID == obj.RegionID);
                    if (result != null)
                    {
                        result.RegionTitle = obj.RegionTitle;
                        result.Code = obj.Code;
                        result.Description = obj.Description;
                        result.RegionID = obj.RegionID;
                        result.RegionTitleInUrdu = obj.RegionTitleInUrdu;
                        result.Description = obj.Description;
                       

                        context.SaveChanges();
                        return result.RegionID;
                    }
                    return result.RegionID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    var result = context.Gen_RegoinInfo.SingleOrDefault(x => x.RegionID == id);
                    if (result != null)
                    {
                        context.Gen_RegoinInfo.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public Gen_RegoinInfo getAlldataByID(int id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_RegoinInfo.SingleOrDefault(x => x.RegionID == id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public List<Gen_RegoinInfo> GetAllRegionInfo()
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    return context.Gen_RegoinInfo.ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public List<Gen_RegoinInfo> checkDuplicate(int id, string title)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    if (id > 0)
                    {
                        return context.Gen_RegoinInfo.Where(x => x.RegionTitle == title && x.RegionID != id).ToList();

                    }
                    else
                    {
                        return context.Gen_RegoinInfo.Where(x => x.RegionTitle == title).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


    }
}
