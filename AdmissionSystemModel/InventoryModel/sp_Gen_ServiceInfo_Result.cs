//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventorySystemModel.InventoryModel
{
    using System;
    
    public partial class sp_Gen_ServiceInfo_Result
    {
        public int ServiceID { get; set; }
        public Nullable<int> Code { get; set; }
        public Nullable<System.DateTime> EntryDate { get; set; }
        public Nullable<int> Gen_Party_ID { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> GrossAmount { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
    }
}
