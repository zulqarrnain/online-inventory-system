//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventorySystemModel.InventoryModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Inv_ProductCategoryInfo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Inv_ProductCategoryInfo()
        {
            this.Inv_ProductInfo = new HashSet<Inv_ProductInfo>();
            this.Inv_ProductSubCategoryInfo = new HashSet<Inv_ProductSubCategoryInfo>();
        }
    
        public int ProductCategoryID { get; set; }
        public int ProductCategoryCode { get; set; }
        public string ProductCategoryTitle { get; set; }
        public string ProductCategoryTitleInUrdu { get; set; }
        public bool InActive { get; set; }
        public string Description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inv_ProductInfo> Inv_ProductInfo { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inv_ProductSubCategoryInfo> Inv_ProductSubCategoryInfo { get; set; }
    }
}
