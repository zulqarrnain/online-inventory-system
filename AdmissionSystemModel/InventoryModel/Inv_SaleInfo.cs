//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventorySystemModel.InventoryModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Inv_SaleInfo
    {
        public int SaleID { get; set; }
        public System.DateTime EntryDate { get; set; }
        public int Code { get; set; }
        public string ManualNr { get; set; }
        public string CNICNr { get; set; }
        public string ContactPerson { get; set; }
        public int Branch_ID { get; set; }
        public int Session_ID { get; set; }
        public Nullable<int> GateInNo_ID { get; set; }
        public int Party_ID { get; set; }
        public string Description { get; set; }
        public decimal GrossAmount { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> FreightDiscount { get; set; }
        public Nullable<decimal> FreightExpenses { get; set; }
        public Nullable<decimal> SalesTax { get; set; }
        public Nullable<decimal> ExtraCharges { get; set; }
        public decimal TotalAmount { get; set; }
        public Nullable<decimal> CashPaid { get; set; }
        public System.DateTime DueDate { get; set; }
        public int LoginID { get; set; }
        public string TerminalName { get; set; }
        public string InvoiceType { get; set; }
        public Nullable<bool> Retail_WholeSale { get; set; }
    }
}
