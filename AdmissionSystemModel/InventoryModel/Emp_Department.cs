//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventorySystemModel.InventoryModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Emp_Department
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Emp_Department()
        {
            this.Emp_EmployeesInfo = new HashSet<Emp_EmployeesInfo>();
        }
    
        public int DepID { get; set; }
        public Nullable<int> Code { get; set; }
        public string DepartmentTitle { get; set; }
        public string DepartmentTitleInUrdu { get; set; }
        public Nullable<bool> InActive { get; set; }
        public string Description { get; set; }
        public int GroupAccount_ID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Emp_EmployeesInfo> Emp_EmployeesInfo { get; set; }
    }
}
