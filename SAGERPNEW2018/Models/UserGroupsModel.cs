﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SAGERPNEW2018.Models
{
    public class UserGroupsModel
    {
        string constring = ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;

        public int GroupID { get; set; }
        public string GroupTitle { get; set; }
        public string Description { get; set; }
        public bool Inactive { get; set; }
        public string Entryby { get; set; }
        public DateTime TimeStamp { get; set; }
        public int UserID { get; set; }
        public DataTable dtdetail { get; set; }

        public bool IsView { get; set; }


        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }


        public DataTable selectall(string where = "",string id="")
        {
            SqlConnection con = new SqlConnection(constring);
            DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(@" select *,  case GLUserGroup.Inactive when'1' then'InActive' else 'Active' end as status  from GLUserGroup" + where, con);
                da.Fill(dt);
                return dt;
        }

        public DataTable selectallForm(string where = "",string id="")
        {
            SqlConnection con = new SqlConnection(constring);
            DataTable dt = new DataTable();
            if (id!="")
            {

                SqlDataAdapter da = new SqlDataAdapter(@"  SELECT       GLUserGroupDetail.UserGroupDetailID, GLUserGroupDetail.UserGroupID, GLUserGroupDetail.FormsID, GLUserGroupDetail.Assign, GLUserGroupDetail.IsEdit, GLUserGroupDetail.IsDelete, GLUserGroupDetail.IsPrint, 
                         GLUserGroupDetail.Isnew, UserForms.FormTitle, GLUserGroup.GroupTitle, GLUserGroup.Description, GLUserGroup.Inactive
                            FROM            GLUserGroupDetail INNER JOIN
                         UserForms ON GLUserGroupDetail.FormsID = UserForms.Formid INNER JOIN
                         GLUserGroup ON GLUserGroupDetail.UserGroupID = GLUserGroup.GroupID " + where, con);
                da.Fill(dt);
                return dt;
            }
            else {

                SqlDataAdapter da = new SqlDataAdapter(@"   Select * from UserForms " + where, con);
                da.Fill(dt);
                return dt;
            }
           
        }

        public bool save(UserGroupsModel model)
        {

              string[] userdata = new Login().userinfofromCookie();

            bool check;
            SqlConnection db = new SqlConnection(constring);
            SqlCommand com = new SqlCommand();
            SqlCommand com2 = new SqlCommand();
            SqlTransaction tran;
            db.Open();
            tran = db.BeginTransaction();
            try
            {
                int newID = new int();
                com.Connection = db;
                com.Transaction = tran;
                if (model.GroupID > 0)
                {
                    com.CommandText = @"update GLUserGroup set   GroupTitle='" + model.GroupTitle
                        + "', Description='"+ model.Description + "',Inactive='" + model.Inactive +
                        "',Entryby='"+ model.Entryby + "' ,TimeStamp='" + DateTime.Now + "' ,UserID='" + model.UserID+ "'  where GLUserGroup.GroupID=" + GroupID +"; delete from GLUserGroupDetail where UserGroupID="+ GroupID;
                    com.ExecuteNonQuery();

                }
                else
                {
                   com.CommandText = @" insert into GLUserGroup  ([GroupTitle],[Description],[Inactive],[Entryby] ,[TimeStamp] ,[UserID]) values
                        ('" + model.GroupTitle + "','" + model.Description + "','" + model.Inactive + "','" + userdata[5] + "' ,  '" + DateTime.Now + "','" + userdata[1] + "'); SELECT CAST(scope_identity() AS int); ";
                    newID = (int)com.ExecuteScalar();
                }

                int a = newID > 0 ? newID : model.GroupID;
                com2.Connection = db;
                com2.Transaction = tran;

                foreach (DataRow item in dtdetail.Rows)
                {

                    com2.CommandText = @"insert into GLUserGroupDetail (UserGroupID,FormsID,Assign,IsEdit,IsDelete,IsNew,IsPrint )values 
                            ('" + a + "','" + item["formid"] + "','" + item["Assign"] + "' ,'" + item["edit"] + "','" + item["delete"] + "','" + item["New"] + "','" + item["print"] + "' )";
                    com2.ExecuteNonQuery();
                }

                tran.Commit();
            }
            catch (SqlException)
            {
                tran.Rollback();
                check = false;

            }
            finally
            {
                db.Close();
                check = true;
            }
            return check;
          

        }


        public bool Deletedrecord(int id)
        {
            SqlConnection con = new SqlConnection(constring);
            SqlCommand cmd = new SqlCommand();
            con.Open();
            cmd.Connection = con;
            cmd.CommandText = "delete from GLUserGroup where GroupID=" + id+ ";delete from GLUserGroupDetail where UserGroupID=" + id + "";
            cmd.ExecuteNonQuery();
            con.Close();
            return true;
        }
    }
}