﻿using AdmissionSystemModel.InventoryModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAGERPNEW2018.Models
{
    public class ReportModel
    {
        
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int SessionID { get; set; }
        public int PartID { get; set; }
        public int AdmissionID { get; set; }


        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }


        public bool GenerateChallan(ReportModel obj)
        {
            try
            {
                string[] userdata = new Login().userinfofromCookie();
                using (var context = new TradeProEntities())
                {
                    var results = context.ChallanGenerates.Where(x => x.AdmissionID == obj.AdmissionID && x.PartID == obj.PartID ).FirstOrDefault();
                    if (results != null)
                    {
                        results.PartID = obj.PartID;
                        results.AdmissionID = obj.AdmissionID;
                        results.CreateDate = DateTime.Now;
                        results.IsPaid = false;
                        results.IsPrint = true;
                        results.SessionID = obj.SessionID;
                        context.SaveChanges();
                    }
                    else
                    {
                        ChallanGenerate cl = new ChallanGenerate();
                        cl.AdmissionID = obj.AdmissionID;
                        cl.PartID = obj.PartID;
                        cl.CreateDate = DateTime.Now;
                        cl.DueDate = obj.DateFrom;
                        cl.IsPrint = true;
                        cl.IsPaid = false;

                        cl.USerID = Convert.ToInt32( userdata[1]);
                     var data=   context.ChallanGenerates.Add(cl);
                        context.SaveChanges();

                    }
                    return true;


                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        //public IEnumerable<SelectListItem> LoadGLAccount(string where = "")
        //{
        //    SqlConnection con = new SqlConnection(constring);
        //    DataTable dt = new DataTable();
        //    SqlDataAdapter da = new SqlDataAdapter("select * , Concat(GLCode ,'  --  ',GLTitle) as title from GLChartOFAccount " + where, con);
        //    da.Fill(dt);
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    list.Add(new SelectListItem { Text = "-GL Account-", Value = "0" });
        //    foreach (DataRow item in dt.Rows)
        //    {
        //        list.Add(new SelectListItem { Text = item["title"].ToString(), Value = item["GLCAID"].ToString() });
        //    }

        //    return list;
        //}

        //public IEnumerable<SelectListItem> LoadDimension(string where = "")
        //{
        //    SqlConnection con = new SqlConnection(constring);
        //    DataTable dt = new DataTable();
        //    SqlDataAdapter da = new SqlDataAdapter("select * from dimensionType" + where, con);
        //    da.Fill(dt);
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    list.Add(new SelectListItem { Text = "-Dimension Type-", Value = "0" });
        //    foreach (DataRow item in dt.Rows)
        //    {
        //        list.Add(new SelectListItem { Text = item["dimension"].ToString(), Value = item["TypeID"].ToString() });
        //    }

        //    return list;
        //}


        public DataTable GetDataCHallan(ReportModel obj)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    using (SqlConnection conn = new SqlConnection(context.Database.Connection.ConnectionString.ToString()))
                    using (SqlCommand cmd = new SqlCommand("sp_GetChallanGenerateReport", conn))
                    {
                        cmd.CommandTimeout = 1500;
                        SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                        adapt.SelectCommand.CommandType = CommandType.StoredProcedure;
                        adapt.SelectCommand.Parameters.AddWithValue("@AdmissionID", obj.AdmissionID);
                        adapt.SelectCommand.Parameters.AddWithValue("@SessionID", obj.SessionID);
                        adapt.SelectCommand.Parameters.AddWithValue("@PartID", obj.PartID);
                        
                        DataTable dt = new DataTable();
                        adapt.Fill(dt);
                        return dt;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        public DataTable GetDataAdmission(string id)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    using (SqlConnection conn = new SqlConnection(context.Database.Connection.ConnectionString.ToString()))
                    using (SqlCommand cmd = new SqlCommand("sp_GetAdmissionData", conn))
                    {
                        cmd.CommandTimeout = 1500;
                        SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                        adapt.SelectCommand.CommandType = CommandType.StoredProcedure;
                        adapt.SelectCommand.Parameters.AddWithValue("@AdmissionID", id);
                       

                        DataTable dt = new DataTable();
                        adapt.Fill(dt);
                        return dt;
                    }
                }
             }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable GetDataSessionWiseReport(int PartID,int sessionID)
        {
            try
            {
                using (var context = new TradeProEntities())
                {
                    using (SqlConnection conn = new SqlConnection(context.Database.Connection.ConnectionString.ToString()))
                    using (SqlCommand cmd = new SqlCommand("sp_rptSessionWiseReport", conn))
                    {
                        cmd.CommandTimeout = 1500;
                        SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                        adapt.SelectCommand.CommandType = CommandType.StoredProcedure;
                        adapt.SelectCommand.Parameters.AddWithValue("@sessionId", sessionID);
                        adapt.SelectCommand.Parameters.AddWithValue("@partId", PartID);



                        DataTable dt = new DataTable();
                        adapt.Fill(dt);
                        return dt;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        // public DataTable selectallTrail(string where = "")
        // {
        //     SqlConnection con = new SqlConnection(constring);
        //     DataTable dt = new DataTable();

        //     SqlDataAdapter da = new SqlDataAdapter(@"     
        //                    select a.*, b.dr,b.cr  ,GLCompany.Address,GLCompany.Title,GLCompany.website,GLCompany.Phone from

        //                  (
        //                 select secondParent.* ,  b.GLCAID as thirdParentID ,  b.GLCode as thirdParentCode ,  b.GLTitle as thirdParentTitle  from(
        //                 select  b.GLCAID as SecondParentID ,  b.GLCode as SecondParentCode ,  b.GLTitle as SecondParentTitle ,    parenttable.* from(select A.GLCAID as FirstParentID,A.GLCode as FirstParentCode,a.GLTitle as FirstParentTitle  from GLChartOFAccount a where isParent=0) parenttable 
        //                 left join GLChartOFAccount b on parenttable.FirstParentID=b.isParent) 

        //                 secondParent left join GLChartOFAccount b on b.isParent=secondParent.SecondParentID 
        //                 )  a inner join GLvDetail b on  a.thirdParentID=b.GlAccountID  
        //                 inner join GLvMAIN on GLvMAIN.vID=b.vID 
        //                 inner join GLCompany on GLCompany.Companyid=GLvMAIN.Comp_Id




        //" + where, con);

        //     da.Fill(dt);
        //     return dt;
        // }


        //public DataTable SearchselectallTrail(string where = "")
        //{
        //    SqlConnection con = new SqlConnection(constring);
        //    DataTable dt = new DataTable();

        //    SqlDataAdapter da = new SqlDataAdapter(@"
        //                   select a.*, b.dr,b.cr  from

        //                 (
        //                select secondParent.* ,  b.GLCAID as thirdParentID ,  b.GLCode as thirdParentCode ,  b.GLTitle as thirdParentTitle  from(
        //                select  b.GLCAID as SecondParentID ,  b.GLCode as SecondParentCode ,  b.GLTitle as SecondParentTitle ,    parenttable.* from(select A.GLCAID as FirstParentID,A.GLCode as FirstParentCode,a.GLTitle as FirstParentTitle  from GLChartOFAccount a where isParent=0) parenttable 
        //                inner join GLChartOFAccount b on parenttable.FirstParentID=b.isParent) 

        //                secondParent inner join GLChartOFAccount b on b.isParent=secondParent.SecondParentID 
        //                )  a inner join GLvDetail b on  a.thirdParentID=b.GlAccountID  
        //                inner join GLvMAIN on GLvMAIN.vID=b.vID 
        //           " + where, con);

        //    da.Fill(dt);
        //    return dt;
        //}




        //    public DataTable selectall(string where = "", string date = "")
        //    {
        //        SqlConnection con = new SqlConnection(constring);
        //        DataTable dt = new DataTable();

        //        SqlDataAdapter da = new SqlDataAdapter(@"sELECT        GLvDetail.GlAccountID, GLvDetail.DTypeID, GLvDetail.DvaluesID, GLvDetail.Narration, GLvDetail.ChequeNo, GLvDetail.ChequeDate, GLvDetail.dr, GLvDetail.cr, GLChartOFAccount.GLCode, GLChartOFAccount.GLTitle, 
        //                     GLvMAIN.vDate,ISNULL(dimensionValues.DValue, '') as dimensionValues , GLvMAIN.vNO, GLvMAIN.vType, GLvMAIN.vID, GLvMAIN.Comp_Id, GLvMAIN.FiscalID, GLvMAIN.vUserID, aaa.dr as GroupTotalDr, aaa.Cr as GroupTotalCr, GLVoucherType.Title 
        //                ,	 format( GLvMAIN.vDate,'dd-MMMM-yyyy') as vdate , ( GLvDetail.dr -GLvDetail.cr + blance.asae) as runingbal, 


        //	case  
        //	when  blance.asae > 0  then CONCAT(  blance.asae,' Dr') else CONCAT(blance.asae ,' Cr' )  end as openingbalnce,
        //GLCompany.Title AS company,GLCompany.Address, GLCompany.Email, GLCompany.Phone, GLCompany.website
        //	 FROM          GLvMAIN INNER JOIN

        //                  GLCompany ON GLvMAIN.Comp_Id = GLCompany.Companyid  INNER JOIN
        //                     GLvDetail ON GLvMAIN.vID = GLvDetail.vID INNER JOIN
        //                     GLChartOFAccount ON GLvDetail.GlAccountID = GLChartOFAccount.GLCAID LEFT OUTER JOIN
        //                     dimensionValues ON GLvDetail.DvaluesID = dimensionValues.DValuesID left outer join 

        //		    GLVoucherType ON GLvMAIN.vType = GLVoucherType.Voucherid INNER JOIN
        //		 (SELECT        GLvDetail.GlAccountID,sum( CONVERT(numeric, GLvDetail.dr)) as dr,sum( CONVERT(numeric, GLvDetail.cr)) as Cr


        //		  FROM  GLvMAIN INNER JOIN
        //                     GLvDetail ON GLvMAIN.vID = GLvDetail.vID 
        //		GROUP BY GLvDetail.GlAccountID
        //	 )aaa on aaa.GlAccountID= GLvDetail.GlAccountID  inner join

        //	 (SELECT        GLvDetail.GlAccountID,sum( CONVERT(numeric, GLvDetail.dr)) as dr, sum( CONVERT(numeric, GLvDetail.cr)) as Cr,

        //	   ( sum( CONVERT(numeric, GLvDetail.dr)) )- (sum( CONVERT(numeric, GLvDetail.cr)))as asae

        //	FROM            GLvMAIN INNER JOIN
        //							 GLvDetail ON GLvMAIN.vID = GLvDetail.vID 


        //							 where  GLvMAIN.vDate < '" + date + @"'
        //	GROUP BY GLvDetail.GlAccountID) blance  on blance.GlAccountID=GLvDetail.GlAccountID " + where, con);

        //        da.Fill(dt);
        //        return dt;
        //    }


        //   public DataTable Searchselectall(string where = "", string date="")
        //   {
        //       SqlConnection con = new SqlConnection(constring);
        //       DataTable dt = new DataTable();

        //           SqlDataAdapter da = new SqlDataAdapter(@"sELECT        GLvDetail.GlAccountID, GLvDetail.DTypeID, GLvDetail.DvaluesID, GLvDetail.Narration, GLvDetail.ChequeNo, GLvDetail.ChequeDate, GLvDetail.dr, GLvDetail.cr, GLChartOFAccount.GLCode, GLChartOFAccount.GLTitle, 
        //                    GLvMAIN.vDate,ISNULL(dimensionValues.DValue, '') as dimensionValues , GLvMAIN.vNO, GLvMAIN.vType, GLvMAIN.vID, GLvMAIN.Comp_Id, GLvMAIN.FiscalID, GLvMAIN.vUserID, aaa.dr as GroupTotalDr, aaa.Cr as GroupTotalCr, GLVoucherType.Title 
        //               ,	 format( GLvMAIN.vDate,'dd-MMMM-yyyy') as vdate , ( GLvDetail.dr -GLvDetail.cr + blance.asae) as runingbal, 


        //case  
        //when  blance.asae > 0  then CONCAT(  blance.asae,' Dr') else CONCAT(blance.asae ,' Cr' )  end as openingbalnce
        // FROM          GLvMAIN INNER JOIN
        //                    GLvDetail ON GLvMAIN.vID = GLvDetail.vID INNER JOIN
        //                    GLChartOFAccount ON GLvDetail.GlAccountID = GLChartOFAccount.GLCAID LEFT OUTER JOIN
        //                    dimensionValues ON GLvDetail.DvaluesID = dimensionValues.DValuesID left outer join 
        //	    GLVoucherType ON GLvMAIN.vType = GLVoucherType.Voucherid INNER JOIN
        //	 (SELECT        GLvDetail.GlAccountID,sum( CONVERT(numeric, GLvDetail.dr)) as dr,sum( CONVERT(numeric, GLvDetail.cr)) as Cr


        //	  FROM  GLvMAIN INNER JOIN
        //                    GLvDetail ON GLvMAIN.vID = GLvDetail.vID 
        //	GROUP BY GLvDetail.GlAccountID
        // )aaa on aaa.GlAccountID= GLvDetail.GlAccountID  inner join

        // (SELECT        GLvDetail.GlAccountID,sum( CONVERT(numeric, GLvDetail.dr)) as dr, sum( CONVERT(numeric, GLvDetail.cr)) as Cr,

        //   ( sum( CONVERT(numeric, GLvDetail.dr)) )- (sum( CONVERT(numeric, GLvDetail.cr)))as asae

        //FROM            GLvMAIN INNER JOIN
        //						 GLvDetail ON GLvMAIN.vID = GLvDetail.vID 

        //						 where  GLvMAIN.vDate < '" + date + @"'
        //GROUP BY GLvDetail.GlAccountID) blance  on blance.GlAccountID=GLvDetail.GlAccountID" + where, con);

        //       da.Fill(dt);
        //       return dt;
        //   }


        //public IEnumerable<SelectListItem> LoadDimensionValue(string where = "")
        //{
        //    SqlConnection con = new SqlConnection(constring);
        //    DataTable dt = new DataTable();
        //    SqlDataAdapter da = new SqlDataAdapter("select * from dimensionValues" + where, con);
        //    da.Fill(dt);
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    list.Add(new SelectListItem { Text = "-Dimension Value-", Value = "0" });
        //    foreach (DataRow item in dt.Rows)
        //    {
        //        list.Add(new SelectListItem { Text = item["DValue"].ToString(), Value = item["DValuesID"].ToString() });
        //    }

        //    return list;
        //}
    }
}



//SqlDataAdapter da = new SqlDataAdapter(@"SELECT        GLvDetail.GlAccountID, GLvDetail.DTypeID, GLvDetail.DvaluesID, GLvDetail.Narration, GLvDetail.ChequeNo, GLvDetail.ChequeDate, GLvDetail.dr, GLvDetail.cr, GLChartOFAccount.GLCode, GLChartOFAccount.GLTitle, 
//                         GLvMAIN.vDate, ISNULL(dimensionValues.DValue, '') AS dimensionValues, GLvMAIN.vNO, GLvMAIN.vID, GLvMAIN.FiscalID, GLvMAIN.vUserID, dimensionValues.DValue, GLvMAIN.vType, GLVoucherType.Title, 
//                         GLCompany.Title AS company,GLCompany.Address, GLCompany.Email, GLCompany.Phone, GLCompany.website
//                            FROM            GLvMAIN INNER JOIN
//                         GLvDetail ON GLvMAIN.vID = GLvDetail.vID INNER JOIN
//                         GLChartOFAccount ON GLvDetail.GlAccountID = GLChartOFAccount.GLCAID INNER JOIN
//                         GLVoucherType ON GLvMAIN.vType = GLVoucherType.Voucherid INNER JOIN
//                         GLCompany ON GLvMAIN.Comp_Id = GLCompany.Companyid LEFT OUTER JOIN
//                         dimensionValues ON GLvDetail.DvaluesID = dimensionValues.DValuesID " + where, con);

//da.Fill(dt);