﻿using InventorySystemModel.InventoryModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace SAGERPNEW2018.Models
{
    public class Login
    {

        string constring = ConfigurationManager.ConnectionStrings["TradeProEntities"].ConnectionString;
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool type { get; set; }
        public string SystemName { get; set; }
        public string IP{ get; set; }


        public void SetDataUser(GLUser user)
        {
            try
            {
        
                HttpCookie aCookie = new HttpCookie("UserCookies");
                aCookie.Values["Userid"] = user.Userid.ToString();
                aCookie.Values["UserName"] = user.UserName;
                aCookie.Values["UserPassword"] = user.UserPassword;
                aCookie.Values["Type"] = user.Type.ToString();
                aCookie.Values["system"] = user.system;
            

                aCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(aCookie);
            }
            catch (Exception)
            { }
        }

        public GLUser GetUser()
        {
            GLUser user = null;
            HttpCookie authCookie = HttpContext.Current.Request.Cookies["UserCookies"];

            if (authCookie != null)
            {
                user = new GLUser();
                user.Userid = Convert.ToInt32((authCookie.Values["Userid"]));
                user.UserName = Convert.ToString(authCookie.Values["UserName"]);
                user.UserPassword = Convert.ToString(authCookie.Values["UserPassword"]);
                user.Type = Convert.ToBoolean(authCookie.Values["Type"]);
                user.system = Convert.ToString(authCookie.Values["system"]);
               


            }
            return user;
        }


        public void SetDataUserRights(GLUser user)
        {
            try
            {

                var ListOfRight = new Login().checkRightUser(user.Userid);
                string myObjectJson = JsonConvert.SerializeObject(ListOfRight,Formatting.Indented);
                HttpCookie cookie = new HttpCookie("RightCookie"); 
                cookie.Value = HttpContext.Current.Server.UrlEncode(myObjectJson);
                HttpContext.Current.Response.Cookies.Add(cookie);


                //                var cookie = new HttpCookie("RightCookie", myObjectJson)
                //{
                //    Expires = DateTime.Now.AddYears(1)
                //};
                //HttpContext.Current.Response.Cookies.Add(cookie);

             

                //HttpCookie aCookie = new HttpCookie("RightCookie");
                //var ListOfRight = new Login().checkRightUser(user.Userid);
                //aCookie.Values["Rights"] = JsonConvert.SerializeObject(ListOfRight);
                //aCookie.Expires = DateTime.Now.AddDays(1);
                //HttpContext.Current.Response.Cookies.Add(aCookie);



                //if (HttpContext.Current.Request.Cookies["UserRightCookie"] == null)
                //{
                //   // var ListOfRight = new Login().checkRightUser(user.Userid);
                //    HttpContext.Current.Response.Cookies.Add(new HttpCookie("UserRightCookie", JsonConvert.SerializeObject(ListOfRight)));
                //}

            }
            catch (Exception)
            { }
        }
        
        public List<sp_GetUserRightByUser_Result> GetDataUserRights()
        {
            try
            {
                HttpContext authCookie = HttpContext.Current;
                if (authCookie != null)
                {

                    return JsonConvert.DeserializeObject<List<sp_GetUserRightByUser_Result>>(Convert.ToString(authCookie.Request.Cookies["RightCookie"]));
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }




        public string[] userinfofromCookie()
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName]; 
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

            string cookiePath = ticket.CookiePath;
            DateTime expiration = ticket.Expiration;
            bool expired = ticket.Expired;
            bool isPersistent = ticket.IsPersistent;
            DateTime issueDate = ticket.IssueDate;
            string name = ticket.Name;
            string userData = ticket.UserData;
            int version = ticket.Version;
            string[] a= name.Split('|');
            return a;
        }

        public DataTable checkRightUser(string where="")
        {
            using (var context = new TradeProEntities())
            {
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(@"SELECT     GLUser.type,  GLUser.Userid, GLUser.UserName, GLUser.UserPassword, GLUser.GroupID, GLUser.Active, GLUserGroup.GroupTitle, GLUserGroup.Description, GLUserGroup.Inactive, GLUserGroupDetail.Assign, GLUserGroupDetail.IsEdit, 
                         GLUserGroupDetail.IsDelete, GLUserGroupDetail.IsPrint, GLUserGroupDetail.Isnew, UserForms.FormTitle, UserForms.Formid
                            FROM            GLUser INNER JOIN
                         GLUserGroup ON GLUserGroup.GroupID = GLUser.GroupID INNER JOIN
                         GLUserGroupDetail ON GLUserGroupDetail.UserGroupID = GLUserGroup.GroupID INNER JOIN
                         UserForms ON UserForms.Formid = GLUserGroupDetail.FormsID " + where + "", context.Database.Connection.ConnectionString.ToString()
                             );
                da.Fill(dt);
                return dt;
            }
        }


        public List<sp_GetUserRightByUser_Result> checkRightUser(int UserID )
        {
            using (var context = new TradeProEntities())
            {
              return  context.sp_GetUserRightByUser(UserID).ToList();
            }
        }
        public void ExpireUserCookiee()
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies["UserCookies"];
            if (authCookie != null)
            {
                authCookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(authCookie);
            }
        }

        //public bool MenuExist(int UserRoleId, string ActionName, string ControllerName)
        //{
        //    try
        //    {
        //        using (var context = new SalesOperationsEntities())
        //        {
        //            var configurationId = context.MenusConfigurations.SingleOrDefault(c => c.ActionName.Equals(ActionName) && c.ControllerName.Equals(ControllerName)).ConfigurationId;

        //            if (context.MenusRights.Where(m => m.RoleId == UserRoleId && m.ConfigurationId == configurationId).ToList().Count() > 0)
        //            {
        //                return true;
        //            }
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().LogError(ex);
        //        return false;
        //    }
        //}


    }
}


//using SalesOperations.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace SalesOperations.Filters
//{
//    public class CookieHandler : System.Web.HttpApplication
//    {
//        public void SetUser(User user)
//        {
//            try
//            {
//                HttpCookie aCookie = new HttpCookie("UserCookie");

//                aCookie.Values["UserId"] = new SalesOperations.Common.SecurityManager().EncodeString(user.UserId.ToString());
//                aCookie.Values["LoginName"] = user.LoginName;
//                aCookie.Values["LoginPassword"] = user.LoginPassword;
//                aCookie.Values["UserRoleId"] = user.UserRoleId.ToString();
//                aCookie.Values["FirstName"] = user.FirstName;
//                aCookie.Values["LastName"] = user.LastName;
//                aCookie.Expires = DateTime.Now.AddDays(1);
//                HttpContext.Current.Response.Cookies.Add(aCookie);
//            }
//            catch (Exception)
//            { }
//        }

//        public void SetDistributor(Distributor distributor)
//        {
//            try
//            {
//                HttpCookie aCookie = new HttpCookie("DistributorCookie");
//                aCookie.Values["DistributorId"] = distributor.Distributor_Id.ToString();
//                aCookie.Values["Name"] = distributor.Name;
//                aCookie.Expires = DateTime.Now.AddDays(1);
//                HttpContext.Current.Response.Cookies.Add(aCookie);
//            }
//            catch (Exception)
//            { }
//        }


//        public User GetUser()
//        {
//            User user = null;
//            HttpCookie authCookie = HttpContext.Current.Request.Cookies["UserCookie"];

//            if (authCookie != null)
//            {
//                user = new User();
//                user.UserId = Convert.ToInt32(new SalesOperations.Common.SecurityManager().DecodeString(authCookie.Values["UserId"]));
//                user.LoginName = Convert.ToString(authCookie.Values["LoginName"]);
//                user.LoginPassword = Convert.ToString(authCookie.Values["LoginPassword"]);
//                user.UserRoleId = Convert.ToInt32(authCookie.Values["UserRoleId"]);
//                user.FirstName = Convert.ToString(authCookie.Values["FirstName"]);
//                user.LastName = Convert.ToString(authCookie.Values["LastName"]);
//            }
//            return user;
//        }

//        public Distributor GetDistributor()
//        {
//            Distributor distributor = null;
//            HttpCookie authCookie = HttpContext.Current.Request.Cookies["DistributorCookie"];

//            if (authCookie != null)
//            {
//                distributor = new Distributor();
//                distributor.Distributor_Id = Convert.ToInt32(authCookie.Values["DistributorId"]);
//                distributor.Name = Convert.ToString(authCookie.Values["Name"]);

//            }
//            return distributor;
//        }

//        public void ExpireUserCookiee()
//        {
//            HttpCookie authCookie = HttpContext.Current.Request.Cookies["UserCookie"];
//            if (authCookie != null)
//            {
//                authCookie.Expires = DateTime.Now.AddDays(-1);
//                HttpContext.Current.Response.Cookies.Add(authCookie);
//            }
//        }

//        public void ExpireDistributorCookiee()
//        {
//            HttpCookie authCookie = HttpContext.Current.Request.Cookies["DistributorCookie"];
//            if (authCookie != null)
//            {
//                authCookie.Expires = DateTime.Now.AddDays(-1);
//                HttpContext.Current.Response.Cookies.Add(authCookie);
//            }
//        }
//    }
//}