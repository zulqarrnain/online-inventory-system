﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SAGERPNEW2018.Models
{
    public class GLUserModel
    {
        string constring = ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
        public enum GLBranchesModelField
        {
            Companyid,
            Title,
            ShortTitle,
        }

        public bool IsView;
        public int Companyid { get; set; }
        public string Title { get; set; }
        public string ShortTitle { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }



        public DataTable selectall(string where = "")
        {
            SqlConnection con = new SqlConnection(constring);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from [dbo].[GLUser] " + where, con);
            da.Fill(dt);
            return dt;
        }
    }

}