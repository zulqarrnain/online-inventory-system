﻿using AdmissionSystemModel.InventoryModel;
using Newtonsoft.Json;
using SAGERPNEW2018.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAGERPNEW2018.Controllers
{
    public class PackageInfoController : Controller
    {
        // GET: PackageInfo
        [Authorize]
        public ActionResult Index()
        {
            StudentPackageInfo a = new StudentPackageInfo();

            DataTable dt = loadrighta();
            if (dt.Rows.Count > 0)
            {
                a.Isdelete = Convert.ToBoolean(dt.Rows[0]["Isdelete"]);
                a.IsNew = Convert.ToBoolean(dt.Rows[0]["IsNew"]);
                a.IsPrint = Convert.ToBoolean(dt.Rows[0]["IsPrint"]);
                a.Isedit = Convert.ToBoolean(dt.Rows[0]["Isedit"]);
                a.IsAsign = Convert.ToBoolean(dt.Rows[0]["Assign"]);
                if (!a.IsAsign)
                {
                    TempData["NORights"] = "You Have No Rights To Perform This Action";
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(a);
        }
        [Authorize]
        public ActionResult create(StudentPackageInfo a)
        {
            DataTable dt = loadrighta();
            if (dt.Rows.Count > 0)
            {
                a.Isdelete = Convert.ToBoolean(dt.Rows[0]["Isdelete"]);
                a.IsNew = Convert.ToBoolean(dt.Rows[0]["IsNew"]);
                a.IsPrint = Convert.ToBoolean(dt.Rows[0]["IsPrint"]);
                a.Isedit = Convert.ToBoolean(dt.Rows[0]["Isedit"]);
                a.IsAsign = Convert.ToBoolean(dt.Rows[0]["Assign"]);
                if (!a.IsAsign)
                {
                    TempData["NORights"] = "You Have No Rights To Perform This Action";
                    return RedirectToAction("Index", "Home");
                }
            }

            return View(a);
        }
        [Authorize]
        public ActionResult delete(int id)
        {
            StudentPackageInfo a = new StudentPackageInfo();
            bool c = a.DeleteData(id);
            if (c)
            {
                return RedirectToAction("Index");

            }
            TempData["Dependancy"] = "This Record Using In Another Table";
            return RedirectToAction("Index");
        }
        [Authorize]
        public ActionResult Edit(string id)
        {
            string[] ID = id.Split('|');

            StudentPackageInfo a = new StudentPackageInfo();
            var oobj = a.getAlldataByID(Convert.ToInt32(ID[0]));

            if (ID[1] == "0")
            {
                a.IsView = true;
            }
            return View("create", oobj);
        }

        [Authorize]
        public ActionResult Save(StudentPackageInfo model)
        {
            int check;
            if (model.PackageID > 0)
            {
                check = check = model.UpdateData(model);
            }
            else
            {

                check = model.addata(model);
            }

            if (check > 0)
            {
                return RedirectToAction("Index");

            }
            return RedirectToAction("create", model);
        }

        public JsonResult Duplicate(string Name, int ID)
        {
            string json = "";
            var list = new StudentPackageInfo().checkDuplicate(ID, Name);
            if (list.Count() > 0)
            {
                json = " Duplicate Record Found ";
            }
            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }

        private DataTable loadrighta()
        {
            string[] userdata = new Login().userinfofromCookie();
            DataTable dtright = new Login().checkRightUser(" where GLUser.Userid='" + userdata[1] + "' and UserForms.Formid='30' ");
            return dtright;
        }
    }
}