﻿using InventorySystemModel.InventoryModel;
using Newtonsoft.Json;
using SAGERPNEW2018.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAGERPNEW2018.Controllers
{
    public class BranchInfoController : Controller
    {
        // GET: BranchInfo
        [UserRightFilters]
        public ActionResult Index()
        {

           
                Gen_BranchInfo a = new Gen_BranchInfo();
                a.IsNew = Convert.ToBoolean(TempData["IsNew"]);
                a.Isedit = Convert.ToBoolean(TempData["IsEdit"]);
                a.Isdelete = Convert.ToBoolean(TempData["IsDelete"]);
                a.IsPrint = Convert.ToBoolean(TempData["IsPrint"]);
                return View(a);
         


        }
        [UserRightFilters]
        public ActionResult create(Gen_BranchInfo a)
        {
           
                a.IsNew = Convert.ToBoolean(TempData["IsNew"]);
                a.Isedit = Convert.ToBoolean(TempData["IsEdit"]);
                a.Isdelete = Convert.ToBoolean(TempData["IsDelete"]);
                a.IsPrint = Convert.ToBoolean(TempData["IsPrint"]);
                a.LogoPath = "~/AppFiles/Images/placeholder-avatar.jpg";

                return View(a);
           

        }

        public ActionResult delete(int id)
        {
           
                Gen_BranchInfo a = new Gen_BranchInfo();

                bool c = a.DeleteData(id);
                if (c)
                {
                    return RedirectToAction("Index");

                }
                TempData["Dependancy"] = "This Record Using In Another Table";
                return RedirectToAction("Index");
           


        }

        public ActionResult Edit(string id)
        {
           
                string[] ID = id.Split('|');

                Gen_BranchInfo a = new Gen_BranchInfo();
                var obj = a.getAlldataByID(Convert.ToInt32(ID[0]));
                if (ID[1] == "0")
                {
                    a.IsView = true;
                }
                return View("create", obj);

          

        }

      
        public ActionResult Save(Gen_BranchInfo model)
        {
           
                int check;
                if (model.ImageUpload != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(model.ImageUpload.FileName);
                    string extension = Path.GetExtension(model.ImageUpload.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    model.LogoPath = "~/AppFiles/Images/" + fileName;
                    model.ImageUpload.SaveAs(Path.Combine(Server.MapPath("~/AppFiles/Images/"), fileName));
                }

                if (model.BranchID > 0)
                {
                    check = check = model.UpdateData(model);
                }
                else
                {

                    check = model.addata(model);
                }

                if (check > 0)
                {
                    return RedirectToAction("Index");

                }
                return RedirectToAction("create", model);

           
        }

        public ActionResult Duplicate(string Name, Int32 ID)
        {
            
                string json = "";
                var list = new Gen_BranchInfo().checkDuplicate(ID, Name);
                if (list.Count() > 0)
                {
                    json = " Duplicate Record Found ";
                }
                return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);



        }
    }
}