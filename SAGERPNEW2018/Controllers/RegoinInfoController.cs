﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SAGERPNEW2018.Models;
using InventorySystemModel.InventoryModel;
using SAGERPNEW2018.Filters;
using Newtonsoft.Json;

namespace SAGERPNEW2018.Controllers
{
    public class RegoinInfoController : Controller
    {
        // GET: RegoinInfo
        [UserRightFilters]
        public ActionResult Index()
        {
            try
            {
                Gen_RegoinInfo a = new Gen_RegoinInfo();
                a.IsNew = Convert.ToBoolean(TempData["IsNew"]);
                a.Isedit = Convert.ToBoolean(TempData["IsEdit"]);
                a.Isdelete = Convert.ToBoolean(TempData["IsDelete"]);
                a.IsPrint = Convert.ToBoolean(TempData["IsPrint"]);
                return View(a);
            }
            catch (Exception ex)
            {
                return View();
            }

        }

        [UserRightFilters]
        public ActionResult create(Gen_RegoinInfo a)
        {
            try
            {
                a.IsNew = Convert.ToBoolean(TempData["IsNew"]);
                a.Isedit = Convert.ToBoolean(TempData["IsEdit"]);
                a.Isdelete = Convert.ToBoolean(TempData["IsDelete"]);
                a.IsPrint = Convert.ToBoolean(TempData["IsPrint"]);
               

                return View(a);
            }
            catch (Exception ex)
            {

                return View(a);
            }

        }
        [UserRightFilters]
        public ActionResult delete(int id)
        {
            try
            {
                Gen_RegoinInfo a = new Gen_RegoinInfo();

                bool c = a.DeleteData(id);
                if (c)
                {
                    return RedirectToAction("Index");

                }
                TempData["Dependancy"] = "This Record Using In Another Table";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }


        }
        [UserRightFilters]
        public ActionResult Edit(string id)
        {
            try
            {
                string[] ID = id.Split('|');

                Gen_RegoinInfo a = new Gen_RegoinInfo();
                var obj = a.getAlldataByID(Convert.ToInt32(ID[0]));
                if (ID[1] == "0")
                {
                    a.IsView = true;
                }
                return View("create", obj);

            }
            catch (Exception ex)
            {
                return View("create");
            }

        }
        public ActionResult Save(Gen_RegoinInfo model)
        {
            try
            {
                int check;
                model.InActive = true;

                if (model.RegionID > 0)
                {
                    check = check = model.UpdateData(model);
                }
                else
                {

                    check = model.addata(model);
                }

                if (check > 0)
                {
                    return RedirectToAction("Index");

                }
                return RedirectToAction("create", model);

            }
            catch (Exception ex)
            {

                return View("create");
            }
        }

        public ActionResult Duplicate(string Name, Int32 ID)
        {
            try
            {
                string json = "";
                var list = new Gen_RegoinInfo().checkDuplicate(ID, Name);
                if (list.Count() > 0)
                {
                    json = " Duplicate Record Found ";
                }
                return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {


                return Json(JsonConvert.SerializeObject("Error"), JsonRequestBehavior.AllowGet);

            }


        }
    }
}