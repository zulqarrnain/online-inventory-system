﻿using InventorySystemModel.InventoryModel;
using Newtonsoft.Json;
using SAGERPNEW2018.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAGERPNEW2018.Controllers
{
    public class PartyInfoController : Controller
    {
        // GET: PartyInfo
        [UserRightFilters]
        public ActionResult Index()
        {


            Gen_PartyInfo a = new Gen_PartyInfo();
            a.IsNew = Convert.ToBoolean(TempData["IsNew"]);
            a.Isedit = Convert.ToBoolean(TempData["IsEdit"]);
            a.Isdelete = Convert.ToBoolean(TempData["IsDelete"]);
            a.IsPrint = Convert.ToBoolean(TempData["IsPrint"]);
            return View(a);


        }
        [UserRightFilters]
        public ActionResult create(Gen_PartyInfo a)
        {

            a.IsNew = Convert.ToBoolean(TempData["IsNew"]);
            a.Isedit = Convert.ToBoolean(TempData["IsEdit"]);
            a.Isdelete = Convert.ToBoolean(TempData["IsDelete"]);
            a.IsPrint = Convert.ToBoolean(TempData["IsPrint"]);
            a.LogoPath = "~/AppFiles/Images/placeholder-avatar.jpg";

            return View(a);


        }

        public ActionResult delete(int id)
        {

            Gen_PartyInfo a = new Gen_PartyInfo();

            bool c = a.DeleteData(id);
            if (c)
            {
                return RedirectToAction("Index");

            }
            TempData["Dependancy"] = "This Record Using In Another Table";
            return RedirectToAction("Index");



        }


        public ActionResult Edit(string id)
        {

            string[] ID = id.Split('|');

            Gen_PartyInfo a = new Gen_PartyInfo();
            var obj = a.getAlldataByID(Convert.ToInt32(ID[0]));
            if (ID[1] == "0")
            {
                a.IsView = true;
            }
            return View("create", obj);



        }

        public ActionResult Save(Gen_PartyInfo model)
        {

            int check;
            if (model.ImageUpload != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(model.ImageUpload.FileName);
                string extension = Path.GetExtension(model.ImageUpload.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                model.LogoPath = "~/AppFiles/Images/" + fileName;
                model.ImageUpload.SaveAs(Path.Combine(Server.MapPath("~/AppFiles/Images/"), fileName));
            }

            if (model.PartyID > 0)
            {
                check = check = model.UpdateData(model);
            }
            else
            {

                check = model.addata(model);
            }

            if (check > 0)
            {
                return RedirectToAction("Index");

            }
            return RedirectToAction("create", model);


        }

        public ActionResult Duplicate(string Name, Int32 ID)
        {

            string json = "";
            var list = new Gen_PartyInfo().checkDuplicate(ID, Name);
            if (list.Count() > 0)
            {
                json = " Duplicate Record Found ";
            }
            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);

        }

        public JsonResult FetchTerritory(int ID)
        {
            try
            {
                var TerritoryList = new Gen_PartyInfo().GetTerritory(ID);
                var result = TerritoryList.Select(x => new SelectListItem { Value = x.Region_ID.ToString(), Text = x.TerritoryTitle.ToString() }).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public JsonResult FetchTown(int ID)
        {
            try
            {
                var TownList = new Gen_PartyInfo().GetTown(ID);
                var result = TownList.Select(x => new SelectListItem { Value = x.TownID.ToString(), Text = x.TownTitle.ToString() }).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public JsonResult FetchSector(int ID)
        {
            try
            {
                var SectorList = new Gen_PartyInfo().GetSector(ID);
                var result = SectorList.Select(x => new SelectListItem { Value = x.SectorID.ToString(), Text = x.SectorTitle.ToString() }).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}