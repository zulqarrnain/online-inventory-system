﻿using Newtonsoft.Json;
using AdmissionSystemModel.InventoryModel;
using SAGERPNEW2018.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using Microsoft.Reporting.WebForms;

namespace SAGERPNEW2018.Controllers
{
    public class GLReportsController : Controller
    {
        // GET: GLReports //
        public ActionResult SessionWiseReport()
        {
            ReportModel a = new ReportModel();
            DataTable dt = loadrighta();
            if (dt.Rows.Count > 0)
            {
                a.Isdelete = Convert.ToBoolean(dt.Rows[0]["Isdelete"]);
                a.IsNew = Convert.ToBoolean(dt.Rows[0]["IsNew"]);
                a.IsPrint = Convert.ToBoolean(dt.Rows[0]["IsPrint"]);
                a.Isedit = Convert.ToBoolean(dt.Rows[0]["Isedit"]);
                a.IsAsign = Convert.ToBoolean(dt.Rows[0]["Assign"]);
                if (!a.IsAsign)
                {
                    TempData["NORights"] = "You Have No Rights To Perform This Action";
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(a);
        }

        [HttpPost]
        public ActionResult SessionWiseReport(ReportModel model)
        {
            string[] userdata = new Login().userinfofromCookie();
            try
            {
                ReportDocument rptH = new ReportDocument();
                rptH.Load(Path.Combine(Server.MapPath("~/Reports"), "SessionReport.rpt"));
                var dt = new ReportModel().GetDataSessionWiseReport(model.PartID,model.SessionID);
                var host = new UriBuilder(Request.Url.Scheme, Request.Url.Host, Request.Url.Port);
                foreach (DataRow item in dt.Rows)
                {
                    item["PhotoPath"] = host + item["PhotoPath"].ToString().TrimStart('~');

                }
                rptH.Database.Tables[0].SetDataSource(dt);
                //rptH.SetDataSource();
                if (dt.Rows.Count > 0)
                {
                    
                   // rptH.SetParameterValue("image",dt.row);

                    // rptH.SetParameterValue("image", path);

                    Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    return File(stream, "application/pdf", "Session Wise Report.pdf");
                }
                else
                {
                    return Content("No Data Found");

                    //  return RedirectToAction("TrailIndex", "GLReports");

                }

            }
            catch (Exception ex)
            {

                throw;
            }

        }





        public ActionResult AdmissionReport(string id , string type)
        {
            string[] userdata = new Login().userinfofromCookie();

            //  string[] response = id.Split('|');
            if (type == "0")
            {
                ReportDocument rptH = new ReportDocument();
                rptH.Load(Path.Combine(Server.MapPath("~/Reports"), "AdmissionReport.rpt"));
                var dt = new ReportModel().GetDataAdmission(id);
                rptH.Database.Tables[0].SetDataSource(dt);
                //rptH.SetDataSource();
                if (dt.Rows.Count > 0)
                {
                    var host = new UriBuilder(Request.Url.Scheme, Request.Url.Host, Request.Url.Port);

                    string path = host + dt.Rows[0]["PhotoPath"].ToString().TrimStart('~');
                    rptH.SetParameterValue("ImagePath", path);

                    Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    return File(stream, "application/pdf", "Admission Information.pdf");
                }
                else
                {
                    return Content("No Data Found");

                    //  return RedirectToAction("TrailIndex", "GLReports");

                }

            }
            else
            {

                ReportDocument rptH = new ReportDocument();
                rptH.Load(Path.Combine(Server.MapPath("~/Reports"), "DetailAddmissionReport.rpt"));
                var dt = new ReportModel().GetDataAdmission(id);
                rptH.Database.Tables[0].SetDataSource(dt);
                //rptH.SetDataSource();
                if (dt.Rows.Count > 0)
                {
                    var host = new UriBuilder(Request.Url.Scheme, Request.Url.Host, Request.Url.Port);

                    string path = host + dt.Rows[0]["PhotoPath"].ToString().TrimStart('~');
                    rptH.SetParameterValue("ImageParm", path);

                    Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    return File(stream, "application/pdf", "Admission Information.pdf");
                }
                else
                {
                    return Content("No Data Found");

                    //  return RedirectToAction("TrailIndex", "GLReports");

                }

            }

          

        }


        public ActionResult ChallanPrint()
        {
            ReportModel a = new ReportModel();
            DataTable dt = loadrighta();
            if (dt.Rows.Count > 0)
            {
                a.Isdelete = Convert.ToBoolean(dt.Rows[0]["Isdelete"]);
                a.IsNew = Convert.ToBoolean(dt.Rows[0]["IsNew"]);
                a.IsPrint = Convert.ToBoolean(dt.Rows[0]["IsPrint"]);
                a.Isedit = Convert.ToBoolean(dt.Rows[0]["Isedit"]);
                a.IsAsign = Convert.ToBoolean(dt.Rows[0]["Assign"]);
                if (!a.IsAsign)
                {
                    TempData["NORights"] = "You Have No Rights To Perform This Action";
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(a);
        }

        [HttpPost]
        public ActionResult ChallanPrint(ReportModel a)
        {
            DataTable dt = new ReportModel().GetDataCHallan(a);
            var check=new ReportModel().GenerateChallan(a);

            ReportDocument rptH = new ReportDocument();
            rptH.Load(Path.Combine(Server.MapPath("~/Reports"), "RptChallan.rpt"));
            rptH.Database.Tables[0].SetDataSource(dt);
            //rptH.SetDataSource();
            if (dt.Rows.Count > 0)
            {
                rptH.SetParameterValue("ChallanDate", a.DateFrom);
                Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf", "Challan.pdf");
            }
            else
            {
                return Content("No Data Found");

                //  return RedirectToAction("TrailIndex", "GLReports");

            }

            return View(a);
        }


        //public ActionResult TrailIndex()
        //{
        //    ReportModel a = new ReportModel();
        //    DataTable dt = loadrighta(2);
        //    if (dt.Rows.Count > 0)
        //    {
        //        a.Isdelete = Convert.ToBoolean(dt.Rows[0]["Isdelete"]);
        //        a.IsNew = Convert.ToBoolean(dt.Rows[0]["IsNew"]);
        //        a.IsPrint = Convert.ToBoolean(dt.Rows[0]["IsPrint"]);
        //        a.Isedit = Convert.ToBoolean(dt.Rows[0]["Isedit"]);
        //        a.IsAsign = Convert.ToBoolean(dt.Rows[0]["Assign"]);
        //        if (!a.IsAsign)
        //        {
        //            TempData["NORights"] = "You Have No Rights To Perform This Action";
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    return View(a);
        //}
        //public ActionResult Report()
        //{
        //    ReportModel a = new ReportModel();
        //    //  return new Rotativa.MVC.ActionAsPdf("Index");
        //    return new Rotativa.MVC.ActionAsPdf("PrintReport",a) { FileName = "TestActionAsPdf.pdf" };
        //}


        //public ActionResult TrailReport(string iniName)
        //{
        //    string[] userdata = new Login().userinfofromCookie();

        //    string[] response = iniName.Split('|');
        //    string where = "where   GLvMAIN.FiscalID='" + userdata[3] + "' and   GLvMAIN.Comp_Id='" + userdata[4] + "' and ";

        //    if (response[0] != "0")
        //    {
        //        where += "    FirstParentID ='" + response[0] + "' and  ";


        //    }

        //    where += " GLvMAIN.vDate  between '" + response[2] + "' and '" + response[3] + "' order by  GLvMAIN.vDate asc ";



        //    ReportDocument rptH = new ReportDocument();
        //    rptH.Load(Path.Combine(Server.MapPath("~/Reports"), "TrailReportWithlevels.rpt"));
        //    DataTable dt = new ReportModel().selectallTrail(where);
        //    rptH.SetDataSource(dt);
        //    if (dt.Rows.Count > 0)
        //    {
        //        rptH.SetParameterValue("CompanyName", dt.Rows[0]["title"]);
        //        rptH.SetParameterValue("Repoetdate", "Date From " + response[2] + " To " + response[3] + "");
        //        rptH.SetParameterValue("Address", "Address: " + dt.Rows[0]["address"] + " || Contact No: " + dt.Rows[0]["Phone"] + "  || Website: " + dt.Rows[0]["Website"] + " ");
        //        rptH.SetParameterValue("Opeartor", userdata[0]);

        //        Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //        return File(stream, "application/pdf");
        //    }
        //    else
        //    {
        //        return Content("No Data Found");

        //        //  return RedirectToAction("TrailIndex", "GLReports");

        //    }

        //}


        //public ActionResult searchdataVoucherTrail(string glName,  string DateFrom, string DateTo)
        //{
        //    string[] userdata = new Login().userinfofromCookie();

        //    string where = " where   GLvMAIN.FiscalID='" + userdata[3] + "' and   GLvMAIN.Comp_Id='" + userdata[4] + "' and ";

        //    if (glName != "0")
        //    {
        //        where += "    FirstParentID ='" + glName + "' and  ";


        //    }

        //    where += " GLvMAIN.vDate  between '" + DateFrom + "' and '" + DateTo + "' order by a.FirstParentID ";




        //    string CheckDup;

        //    DataTable dt = new ReportModel().SearchselectallTrail(where);
        //    CheckDup = JsonConvert.SerializeObject(dt);
        //    return Json(CheckDup, JsonRequestBehavior.AllowGet);


        //}


        //public ActionResult csReport(string iniName)
        //{
        //    string[] userdata = new Login().userinfofromCookie();
        //    bool hide =false;
        //    string[] response=iniName.Split('|');
        //    string where = "where   GLvMAIN.FiscalID='" + userdata[3] + "' and   GLCompany.Companyid='" + userdata[4] + "' and ";

        //    if (response[0] != "0")
        //    {
        //        where += "   GLvDetail.GlAccountID='" + response[0] + "' and  ";
        //        hide = true;

        //    }
        //    if (response[1] != "0")
        //    {
        //        where += "   GLvDetail.DvaluesID='" + response[1] + "' and ";

        //    }


        //    where += " GLvMAIN.vDate  between '" + response[2] + "' and '" + response[3] + "' order by  GLvMAIN.vDate asc ";



        //    ReportDocument rptH = new ReportDocument();
        //    rptH.Load(Path.Combine(Server.MapPath("~/Reports"), "GlReport.rpt"));
        //    DataTable dt = new ReportModel().selectall(where, response[3]);
        //    rptH.SetDataSource(dt);
        //    rptH.SetParameterValue("CompanyName",dt.Rows[0]["Company"]);
        //    rptH.SetParameterValue("ReportDate", "Date From "+response[2]+" To "+response[3]+"");
        //    rptH.SetParameterValue("Address", "Address: " + dt.Rows[0]["address"] + " || Contact No: " + dt.Rows[0]["Phone"] + "  || Website: " + dt.Rows[0]["Website"] + " ");
        //    rptH.SetParameterValue("isTotal", hide);
        //    rptH.SetParameterValue("ReportPrintBy", userdata[0]);

        //    Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    return File(stream, "application/pdf");
        //}

        //public ActionResult PrintReport(ReportModel a)
        //{

        //    return View(a);
        //}

        //public ActionResult searchdataVoucher(string glName, string DTName, string DVName, string DateFrom, string DateTo)
        //{
        //    string[] userdata = new Login().userinfofromCookie();
        //    string where = " where  GLvMAIN.FiscalID='" + userdata[3] + "' and   GLvMAIN.Comp_Id='" + userdata[4] + "' and  ";

        //    if (glName!="0")
        //    {
        //        where += "   GLvDetail.GlAccountID='"+ glName + "' and ";

        //    }
        //    if (DVName != "0")
        //    {
        //        where += "   GLvDetail.DvaluesID='" + DVName + "' and ";

        //    }

        //    where += " GLvMAIN.vDate  between '" + DateFrom + "' and '" + DateTo + "' " ;


        //    string CheckDup;

        //    DataTable dt = new ReportModel().Searchselectall(where, DateTo);
        //    CheckDup = JsonConvert.SerializeObject(dt);
        //    return Json(CheckDup, JsonRequestBehavior.AllowGet);


        //}

        ////1 for gl
        ////2 trail
        //private DataTable loadrighta(int a)
        //{
        //    DataTable dtright = new DataTable();
        //    string[] userdata = new Login().userinfofromCookie();

        //    if (a== 1)
        //    {
        //         dtright = new Login().checkRightUser(" where GLUser.Userid='" + userdata[1] + "'  and UserForms.Formid='14'  ");

        //    }
        //    if(a == 2)
        //    {
        //         dtright = new Login().checkRightUser(" where GLUser.Userid='" + userdata[1] + "'  and UserForms.Formid='13'  ");
        //    }
        //    return dtright;
        //}
        private DataTable loadrighta()
        {
            string[] userdata = new Login().userinfofromCookie();
            DataTable dtright = new Login().checkRightUser(" where GLUser.Userid='" + userdata[1] + "' and UserForms.Formid='31' ");
            return dtright;
        }
    }
}



//USE[OnlineAdmisssionSystem]
//GO

//ALTER TABLE[dbo].[ChallanGenerate]
//DROP CONSTRAINT[DF_ChallanGenerate_IsPaid]
//GO

//ALTER TABLE[dbo].[ChallanGenerate]
//DROP CONSTRAINT[DF_ChallanGenerate_IsPrint]
//GO

///****** Object:  Table [dbo].[ChallanGenerate]    Script Date: 13-Dec-2018 2:33:39 PM ******/
//DROP TABLE[dbo].[ChallanGenerate]
//GO

///****** Object:  Table [dbo].[ChallanGenerate]    Script Date: 13-Dec-2018 2:33:39 PM ******/
//SET ANSI_NULLS ON
//GO

//SET QUOTED_IDENTIFIER ON
//GO

//CREATE TABLE[dbo].[ChallanGenerate](
//	[ChallanID]
//[INT] IDENTITY(1,1) NOT NULL,

//[AdmissionID] [INT]
//NULL,
//	[PartID]
//[INT]
//NULL,
//	[IsPrint]
//[BIT]
//NULL,
//	[IsPaid]
//[BIT]
//NULL,
//	[USerID]
//[INT]
//NULL,
//	[CreateDate]
//[DATE]
//NULL,
// CONSTRAINT[PK_ChallanGenerate] PRIMARY KEY CLUSTERED
//(
//   [ChallanID] ASC
//)WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]
//) ON[PRIMARY]

//GO

//ALTER TABLE[dbo].[ChallanGenerate]
//ADD CONSTRAINT[DF_ChallanGenerate_IsPrint]  DEFAULT((0)) FOR[IsPrint]
//GO

//ALTER TABLE[dbo].[ChallanGenerate]
//ADD CONSTRAINT[DF_ChallanGenerate_IsPaid]  DEFAULT((0)) FOR[IsPaid]
//GO


